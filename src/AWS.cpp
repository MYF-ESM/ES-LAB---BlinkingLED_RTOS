/**
 * ESP32 AWS Library
 * 
 * Functions to get the crawler coordinates from the Camera over AWS IoT
 * 
 * Authors: Vipul Deshpande, Jaime Burbano
 */


/*
  Copyright 2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
  Permission is hereby granted, free of charge, to any person obtaining a copy of this
  software and associated documentation files (the "Software"), to deal in the Software
  without restriction, including without limitation the rights to use, copy, modify,
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include "secrets.h"
#include <WiFiClientSecure.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>
#include "WiFi.h"
#include "AWS.h"
#include "globals.h" // contains xt, yt, xc, yc, crawler_angle
#include <string>       
#include <iostream>     
#include <sstream>

/* The MQTT topics that this device should publish/subscribe to */
#define AWS_IOT_SUBSCRIBE_TOPIC_T "esp32/target"
#define AWS_IOT_SUBSCRIBE_TOPIC_R "esp32/rover"

WiFiClientSecure net = WiFiClientSecure();
MQTTClient client = MQTTClient(256);

String s_xt, s_yt, s_xc, s_yc, s_crawler_angle;
int xt = 0, yt = 0, xc = 0, yc = 0, crawler_angle = 0;

myawsclass::myawsclass() {

}


String getValue(String& data, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==',' || data.charAt(i)==')' || data.charAt(i)==' ' || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i==maxIndex) ? i+1 : i;
    }
  }
  return data.substring(strIndex[0], strIndex[1]);
}




void messageHandler(String &topic, String &payload) {
      StaticJsonDocument<200> doc;
      deserializeJson(doc, payload);
      String message_target = doc["target"]; // (405, 293)
      String message_crawler = doc["rover"]; // {23: [(241, 279), 42]}
      
      message_target = message_target.substring(1,message_target.length()-1); // 405, 293
      message_crawler = message_crawler.substring(7,message_crawler.length()-2); // 241, 279), 42
      
      /*Extract only if the substring starts with numbers -> avoid unnecessary extractions*/
      if(message_target.charAt(0)== '1' || message_target.charAt(0)=='2' || message_target.charAt(0)=='3' || message_target.charAt(0)=='4' || message_target.charAt(0)=='5' || message_target.charAt(0)=='6' || message_target.charAt(0)=='7' || message_target.charAt(0)=='8' || message_target.charAt(0)=='9'){
        s_xt = getValue(message_target, 0); // extract ...
        xt = s_xt.toInt(); // convert ...
        /*Serial.printf("Xt as an int: %d", xt);
        Serial.println();*/
        s_yt = getValue(message_target, 2);
        yt = s_yt.toInt();
        /*Serial.printf("Yt as an int: %d", yt);
        Serial.println();*/
      }
      if(message_crawler.charAt(0)== '1' || message_crawler.charAt(0)=='2' || message_crawler.charAt(0)=='3' || message_crawler.charAt(0)=='4' || message_crawler.charAt(0)=='5' || message_crawler.charAt(0)=='6' || message_crawler.charAt(0)=='7' || message_crawler.charAt(0)=='8' || message_crawler.charAt(0)=='9'){
        s_xc = getValue(message_crawler, 0);
        xc = s_xc.toInt();
        /*Serial.printf("Xc as an int: %d", xc);
        Serial.println();*/
        s_yc = getValue(message_crawler, 2);
        yc = s_yc.toInt();
        /*Serial.printf("Yc as an int: %d", yc);
        Serial.println();*/
        s_crawler_angle = getValue(message_crawler, 5);
        crawler_angle = s_crawler_angle.toInt();
        /*Serial.printf("Carwler angle in degrees: %d", crawler_angle);
        Serial.println();*/
      }
}


/*ORIGINAL messageHandler*/
/*void messageHandler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  StaticJsonDocument<200> doc;
  deserializeJson(doc, payload);
  const char* message = doc["message"];
}*/

void myawsclass::stayConnected() {
  client.loop();
}

void myawsclass::connectAWS() {

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Connecting to Wi-Fi");

  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print("Connecting...!");
  }

  Serial.println("CONNECTED...!");

  /* Configure WiFiClientSecure to use the AWS IoT device credentials */
  net.setCACert(AWS_CERT_CA);
  net.setCertificate(AWS_CERT_CRT);
  net.setPrivateKey(AWS_CERT_PRIVATE);

  /* Connect to the MQTT broker on the AWS endpoint we defined earlier */
  client.begin(AWS_IOT_ENDPOINT, 8883, net);

  /* Create a message handler */
  client.onMessage(messageHandler);

  Serial.println("Connecting to AWS IOT");

  while (!client.connect(THINGNAME)) {
    Serial.print(".");
    delay(100);
  }

  if(!client.connected())
  {
    Serial.println("AWS IoT Timeout!");
    return;
  }

  /* Subscribe to a topic */
  bool target = client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC_T); // checking successful subscriptions
  bool rover = client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC_R);

  if(target) {Serial.println("Successful Subscription to Topic esp32target");}
  if(rover) {Serial.println("Successful Subscription to Topic esp32rover");}


  Serial.println("AWS IoT Connected!");
}


myawsclass awsobject = myawsclass();  /* creating an object of class aws */


