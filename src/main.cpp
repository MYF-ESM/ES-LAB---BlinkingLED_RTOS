#include <Arduino.h>
#include <sensorDriver.h>
#include <motorDriver.h>
#include <AWS.h>
#include <globals.h> // contains xt, yt, xc, yc, crawler_angle


#define left a[0]
#define center a[1] 
#define right a[2]

/*Tasks declarations here:*/
void taskOne( void * parameter); // Sensors' readings Thread
void taskTwo( void * parameter); // To remain connected to AWS

/*Functions declarations here*/
int calculate(int yc,int xc, int yt, int xt);
void rotate(int angle_req);
bool rotation_direction(int angle_req);
void avoid_obstacle();
void maintain_path();

/*Global variables' declaration:*/
int16_t* a; // Array of the sensor readings
int l_min = 120; 
int c_min = 120; 
int r_min = 120; 
int angle_req;
int tolerance = 10; //Accepted absolute deviation between the ideal orientation and the actual orientation of the crawler

void setup(){

  Serial.begin(9600);
  sensorobject.SETUP();
  Serial.println("Sensor Setup complete!");
  motorobject.SETUP();
  Serial.println("Motor Setup complete!");
  awsobject.connectAWS();
  delay(500);
  xTaskCreate(
                    taskOne,          
                    "Sensor",        
                    10000,              
                    NULL,             
                    1,                
                    NULL);
  xTaskCreate(
                    taskTwo,          
                    "AWS",        
                    10000,              
                    NULL,             
                    1,                
                    NULL);
}



void loop()
{
  vTaskDelay(15000); // time i need after connecting the battery to cover the crawler..

  while(1){

  /*Adjusting the crawler's orientation*/
  angle_req = calculate(yc,xc,yt,xt);
  vTaskDelay(50);
  rotate(angle_req);
  vTaskDelay(50);

  
 /*Moving towards the target as long as no obstacles found*/
  while(left>l_min && center>c_min && right>r_min){
    motorobject.forward(180);
    vTaskDelay(200);
    maintain_path();
    vTaskDelay(50);
    }
/*Will reach here only if an obstacle found*/
motorobject.forward(0);
vTaskDelay(50); 
avoid_obstacle();
  }
  



  
  
}



/*In this Task,the sensor readings are recorded*/
void taskOne( void * parameter )
{
  while(1){
  a = sensorobject.reading();
  /*for (int i=0; i<=2 ; i++){
    Serial.println(a[i]);
    }
  Serial.println("------------------------------------");*/
  
  vTaskDelay(20); //till next reading 
  }
  
}



/*In this Task, the crawler stays in connection with AWS to constantly recieve the payloads*/
void taskTwo( void * parameter )
{
  while(1){ 
    awsobject.stayConnected();
     vTaskDelay(10);
  }
}


/*Obstacle avoidance function*/
void avoid_obstacle(){
  while(left<l_min || center<c_min || right<r_min){
    if(center<c_min){
      if(left>right){
        while(center<c_min || right<r_min){
          motorobject.anticlockwise(200);
          vTaskDelay(50);
        }
      }
      else{
        while(center<c_min || left<l_min){
          motorobject.clockwise(200);
          vTaskDelay(50);
        }
      }
    }
    else if(left<l_min){
      while(left<l_min){
        motorobject.clockwise(200);
        vTaskDelay(50);
      }
    }
    else if(right<r_min){
      while(right<r_min){
        motorobject.anticlockwise(200);
        vTaskDelay(50);
      }
    }
    motorobject.forward(180);
    vTaskDelay(1200); // After orientation away from obstacle, move forward A BIT (A BIT is by Trial & Error)
    motorobject.forward(0);
    vTaskDelay(50);
  }
}



/* Function to calculate the angle required for the crawler to follow to reach target.
 * 
 * It determines the angle between crawler and target using relative positions
 * and calculate the final required angle to align green axis to it.
 * 
 * yc,xc : y and x coordinates of the crawler
 * yt,xt : y and x coordinates of the crawler
 * 
*/
int calculate(int yc,int xc, int yt, int xt){
   int theta_abs = atan2(abs(yc-yt),abs(xc-xt))*180/PI;
   if(yc<yt && xc<xt)
   return 360-theta_abs;
   else if(yc<yt && xc>xt)
   return 180+theta_abs;
   else if(yc>yt && xc<xt)
   return theta_abs;
   else return 180-theta_abs;
}



/* Function that rotates the rover according to the calculated orientation
 * 
 * No return
 */

void rotate(int angle_req){
  bool direction = rotation_direction(angle_req);
  if(direction){
    while(abs(crawler_angle - angle_req)>tolerance){
      if(left<l_min || center<c_min || right<r_min){break;}
      motorobject.clockwise(120);
      vTaskDelay(20);
    }
  }
    else{
      while(abs(crawler_angle - angle_req)>tolerance){
        if(left<l_min || center<c_min || right<r_min){break;}
        motorobject.anticlockwise(120);
        vTaskDelay(20);
      }

    }

    motorobject.forward(0);
}



/* Determine the crawler direction of rotation based on required angle
 * 
 * returns: 
 * 0 = counter-clockwise
 * 1 = clockwise
 */
bool rotation_direction(int angle_req){
  int difference = angle_req - crawler_angle;
    if(difference > 0){ // case of positive 
      if(abs(difference)<=180){
        return 0;
      }
      else
      {
        return 1;
      }
      
    }
    else{// case of negative
      if(abs(difference)<=180){
        return 1;
      }
      else
      {
        return 0;
      }
    }
}



/*Maintains the path of the crawler contiuosly*/
void maintain_path(){
  int theta_m = calculate(yc,xc,yt,xt);

  if (abs(crawler_angle - theta_m)>tolerance)
  {
    rotate(theta_m);
  }
  else{/*Nothing i.e continue forward*/}

}








